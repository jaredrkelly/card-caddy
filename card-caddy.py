#!/usr/bin/env python

import os
from colorama import init, Fore, Back, Style
init()




### CONFIG ###
debug = False
low_colour = Fore.RED
#high_colour = Back.GREEN + Fore.BLACK
high_colour = Fore.GREEN
header_colour = Fore.CYAN
colour_threshold_cards = 0.005
##############


### GLOBALS ###

SUITS = {
    "H" : "Hearts",
    "D" : "Diamonds",
    "S" : "Spades",
    "C" : "Clubs"
}

NUM_SUITS = 4
CARDS_IN_DECK = 52
CARD_AVG = NUM_SUITS/CARDS_IN_DECK

NUMBERS = {
    "A" : 1, 
    "K" : 1,
    "Q" : 1,
    "J" : 1,
    10 : 1, 
    9 : 0, 
    8 : 0, 
    7 : 0, 
    6 : -1, 
    5 : -1, 
    4 : -1, 
    3 : -1, 
    2 : -1
}

def clear():
    os.system('cls')

def showTitle():
    print(header("########################################"))
    print(header("##")+"             CARD CADDY             "+header("##"))
    print(header("##")+"              ♠ ♥ ♦ ♣               "+header("##"))
    print(header("########################################\n"))


def showMenu():
    print(header("########################################"))
    print(header("##")+"             MAIN MENU              "+header("##"))
    print(header("##")+"  1. Black Jack (no suit tracking)  "+header("##"))
    print(header("##")+"  0. Exit                           "+header("##"))
    print(header("########################################\n"))
    print("Select Option:")
    
    valid = False
    while not valid:
        user_in = input("> ")
        if user_in == "1":
            playBlackJackSimple()
            valid = True
        elif user_in == "0":
            exit()


def playBlackJackSimple():
    clear()
    showBJSimpleHeader()
    
    print("How many decks are being used?")
    valid = False
    while not valid:
        
        user_in = input("> ")
        try:
            if int(user_in) > 0:
                valid = True
                deck = createSimpleDeck(int(user_in))
                cards_start = int(user_in) * CARDS_IN_DECK
        except ValueError:
            pass

    exit = False
    while not exit:
        clear()
        showBJSimpleHeader()
        showStats(deck, cards_start)
        exit = getCardInput(deck)
    clear()

def getCardInput(deck):
    print("\nEnter dealt cards. Multiple cards can be entered (eg. '2j8a')")
    print("x to Exit")
    user_in = input("> ")
    for idx, char in enumerate(user_in):
        if char.upper() == "X":
            return True
        if char.upper() in deck:
            deck[char.upper()] -= 1
        try:
            if int(char) == 1 and int(user_in[idx+1]) == 0:
                deck[10] -= 1
            if int(char) in deck:
                deck[int(char)] -= 1
        except ValueError:
            pass
    return False



def showBJSimpleHeader():
    print(header("########################################"))
    print(header("##")+"        BLACK JACK (SIMPLE)         "+header("##"))
    print(header("########################################\n"))
    

def createSimpleDeck(num_decks):
    deck = {}
    for num, value in NUMBERS.items():
        deck[num] = num_decks * NUM_SUITS
    
    if debug:
        print(deck)

    return deck

def showStats(deck, cards_start):
    remaining_card_count = 0
    for num, count in deck.items():
        remaining_card_count += count

    high_remaining = deck[10] + deck["J"] + deck["Q"] + deck["K"] + deck["A"]
    neutral_remaining = deck[7] + deck[8] + deck[9]
    low_remaining = deck[2] + deck[3] + deck[4] + deck[5] + deck[6]

    deck_score, deck_score_adj = calcScore(deck, remaining_card_count)
    print("RUNNING COUNT: %s" % deck_score)    # -1 for all highs, +1 for lows

    adv_str = "PLAYER ADVANTAGE: %.2f\n" % deck_score_adj # adjusted for decks remaining.. running count / decks remaining 
    
    if deck_score_adj > 0:
        print(high(adv_str))
    elif deck_score_adj < 0:
        print(low(adv_str))
    else:
        print(adv_str)


    

    print("CARDS USED\t: %s" % (cards_start - remaining_card_count))
    print("CARDS REMAINING\t: %s\n" % remaining_card_count)
    
    #TODO: change to table view
    print("\t\t  % Chance\t# Remaining")
    print("High (10-A)\t: %.0f%%\t\t%s" % (high_remaining/remaining_card_count*100, high_remaining) )
    print("Neutral (7-9)\t: %.0f%%\t\t%s" %  (neutral_remaining/remaining_card_count*100, neutral_remaining) )
    print("Low (2-6)\t: %.0f%%\t\t%s\n" % (low_remaining/remaining_card_count*100, low_remaining)  )

    for num in NUMBERS:
        count = deck[num]
        percent = count / remaining_card_count
        high_thres = CARD_AVG + colour_threshold_cards
        low_thres = CARD_AVG - colour_threshold_cards

        str = "%s\t\t: %.1f%%\t\t%s" % (num, percent*100, count)
    
        if percent >= high_thres:
            print(high(str))
        elif percent <= low_thres:
            print(low(str))
        else:
            print(str)

        


def header(text):
    return (header_colour + text + Style.RESET_ALL)

def high(text):
    return (high_colour + text + Style.RESET_ALL)

def low(text):
    return (low_colour + text + Style.RESET_ALL)

def calcScore(deck, remaining_card_count):
    score = 0
    for card, count in deck.items():
        score += (count * NUMBERS[card])
    return score, (score/(remaining_card_count/CARDS_IN_DECK))

def main():
    clear()
    showTitle()
    while True:
        showMenu()





main()